package STUDIKASUS;

import java.util.ArrayList;
import java.util.Scanner;

public class MahasiswaMain {
    public static void main(String[] args) {
//        membuat kode Arraylist
        ArrayList<Mahasiswa> mahasiswas = new ArrayList<>();
//        membuat kode scanner untuk memasukkan data
        Scanner dyah = new Scanner(System.in);
//        boolean next bernilai true agar program dapat melooping
        boolean next = true;

        while (next) {
            System.out.print("Masukkan nim : ");
            String nim = dyah.nextLine();

            System.out.print("Masukkan nama : ");
            String nama = dyah.nextLine();

            System.out.print("Masukkan alamat : ");
            String alamat = dyah.nextLine();
            
//            membuat kode Arraylist untuk membuat objek Mahasiswa baru dengan data yang dimasukkan oleh pengguna dan menambahkannya ke dalam ArrayList mahasiswas.
            mahasiswas.add(new Mahasiswa(nama, nim, alamat));

            System.out.print("Tambah lagi? (y/n): ");
            String tambah = dyah.nextLine();

            if (tambah.equalsIgnoreCase("n")) {
//                nilai next bernilai false agar berhenti melooping
                next = false;
            }
        }
        
//        mencetak hasil akhir ArrayList mahasiswas
        System.out.println("==================================");
        for (Mahasiswa mahasiswa : mahasiswas) {
            System.out.println(mahasiswa.getNim() + " | " + mahasiswa.getNama() + " | " + mahasiswa.getAlamat());
        }
    }
}
