package STUDIKASUS;

public class Mahasiswa {
    private String nama;
    private String nim;
    private String alamat;

//    bikin constructor dulu
    public Mahasiswa (String nama, String nim, String alamat){
//        penggunaan this untuk menyamakan variabel global dan lokal
        this.nama = nama;
        this.nim = nim;
        this.alamat = alamat;
    }
//    membuat method setter dan getter
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
